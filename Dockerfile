ARG baseimage=registry.gitlab.com/eyeo/docker/abpchromium_gitlab-runner:202004.1
FROM $baseimage

RUN apt-get update \
&&  apt-get install -qy --no-install-recommends \
      git \
      openssh-server \
&&  mkdir /var/run/sshd \
&&  mkdir -m 0700 /root/.ssh/ \
&&  rm -rf /var/lib/apt/lists/*

# Set an environment variable and banner
# so we know what image was used as a base
ARG baseimage
ENV BASEIMAGE=$baseimage
RUN echo "Development image of ${BASEIMAGE}" > /etc/motd

COPY entrypoint.sh /entrypoint.sh

EXPOSE 22
CMD ["bash", "/entrypoint.sh"]