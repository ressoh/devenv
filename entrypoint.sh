# Install user customisations
echo "=============================="
echo "  Installing custom packages"
echo "=============================="
apt-get update
apt-get install -qy python-pip ${ADDITIONAL_PACKAGES}
pip install pre-commit ${ADDITIONAL_PACKAGES_PIP}

# Due to the ssh daemon wiping out any env vars, explicitly put them in the login environment
# https://github.com/docker/docker/issues/2569#issuecomment-27973910
# remove the default vars from the list with `echo $(env | awk -F'=' {'print "^" $1 "="'}) | sed -e 's/ /|/g'`
env | egrep -v "^LS_COLORS=|^SSH_CONNECTION=|^LANG=|^USER=|^PWD=|^HOME=|^SSH_CLIENT=|^SSH_TTY=|^MAIL=|^TERM=|^SHELL=|^SHLVL=|^LOGNAME=|^PATH=|^_=|^HOSTNAME=" \
    >> /etc/environment

echo "==========================="
echo "  Launching ssh server"
echo "==========================="
# Copy ssh keys from /ssh, which should be the users ~/.ssh/ directory.
find /ssh \( -name "*.pub" -or -name "authorized_keys" \) -exec cat {} >> ~/.ssh/authorized_keys \;
chmod 0600 ~/.ssh/authorized_keys
# If we dont have any ssh keys then warn the user
if [ ! -s ~/.ssh/authorized_keys ] ; then
    echo "WARNING: No ssh public keys could be found!"
    echo "You need to copy your public ssh key to the container, something like.."
    echo "  ssh-add -L | docker exec -i ${HOSTNAME} sh -c 'cat >>~/.ssh/authorized_keys'"
fi

# Output ssh config for tunnelling
JUMPHOST=$(getent hosts $(curl -4 --silent ifconfig.io) | awk {'print $2'})
FRIENDLYNAME=$(echo ${BASEIMAGE} | awk -F/ {'print $NF'} | sed -e 's/_.*/-dev/g')
echo """Add something like this to your local ~/.ssh/config to allow direct ssh access...

Host ${FRIENDLYNAME}
    # Set the port defined in your docker-compose.yml
    Port CHANGEME
    StrictHostKeyChecking no
    HostName 127.0.0.1
    User root
    ForwardAgent yes
    ProxyCommand ssh YOUR_USERNAME@${JUMPHOST} -W %h:%p
    # NOTE: Add .exe if youre on windows (https://github.com/microsoft/vscode-remote-release/issues/18)
    # ProxyCommand ssh.exe YOUR_USERNAME@${JUMPHOST} -W %h:%p

    # Want adb access to your local device? Check the README.md
    # RemoteForward 127.0.0.1:5555 IP.OF.YOUR.DEVICE:5555
"""
exec /usr/sbin/sshd -D