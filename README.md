# Devenv Container Tools

This is some basic tooling that allows a developer-friendly version of
an image to be built and run locally. It takes a base image, which should
already have whatever dependencies are required for a given project, adds an ssh
daemon, some custom packages and mount points to allow a developer to ssh into a
container which is identical to its normal build environment.

## Prerequisites

- docker
- [docker-compose](https://docs.docker.com/compose/install/)

## Basic Example with VS Code

Say you want an environment suitable for building [libadblockplus](https://gitlab.com/eyeo/adblockplus/libadblockplus) and you want this environment to run on a remote server because it has more horsepower than your workstation.

*Run the below from your remote server over ssh*
```
# Clone the repo
git clone git@gitlab.com:eyeo/adblockplus/libadblockplus.git
cd libadblockplus
# Get the initial version of the compose file
wget https://gitlab.com/eyeo/distpartners/devenv/-/raw/master/docker-compose.yml
# Add the compose file to your local gitignore (optional)
echo docker-compose.yml >> .git/info/exclude
```
Once the default `docker-compose.yml` is on the server you can change as required. Full details are included as comments in the file but the basics are:

- Change the service name from `devcontainer` to something specific to you. Unfortunately compose files don't support variables in the service names. Something like this should work:  `sed -i -e "s/devcontainer/${USER}_dev/g" docker-compose.yml`

- Change the `port` published by the container (line ~8) to a spare port number on the host. Pick a random number between 1024 and 65000, chances are it will be free but you can check with `netstat -tnl`. I'm gonna pick 3000 for this example. **Do not change the IP address or second port number (22)**

- Change the `baseimage` (around line 14) to reflect the base docker image that your project normally uses to build, this can typically be found in your gitlab config (`grep image: .gitlab-ci.yml`) and a full list of eyeo images can be observed in the [docker project registry](https://gitlab.com/eyeo/docker/container_registry).

- Optional, but recommended. Setup some additional `volumes` so your container has some persistent storage. In this example we have already checked out our project to our home directory so lets mount it under the `/code` directory in the container:  `- ~/libadblockplus:/code:Z` (the `:Z` is to support selinux, if installed on your system)

- Optional. Add some more packages to the container automatically by adding the package names to the `ADDITIONAL_PACKAGES` environment variable.

Our updated `docker-compose.yml` looks like this...

```
version: '3'
services:
  jsault_dev:
    ports:
      - "127.0.0.1:3000:22"
    build:
      context: https://gitlab.com/eyeo/distpartners/devenv.git
      args:
        baseimage: registry.gitlab.com/eyeo/docker/libadblockplus_gitlab-runner:202004.1
    volumes:
      - ~/.ssh:/ssh/:ro,Z
      - ~/libadblockplus:/code:Z
    environment:
      ADDITIONAL_PACKAGES: "vim net-tools nano"
      ADDITIONAL_PACKAGES_PIP: "niet"
```

Now its time to fire it up! Start the container with `docker-compose up --build`, you
will see the container being built, packages being installed etc.
After a minute or so you should see something similar to the below...

```
jsault_dev_1  | ===========================
jsault_dev_1  |   Launching ssh server
jsault_dev_1  | ===========================
jsault_dev_1  | Add something like this to your local ~/.ssh/config to allow direct ssh access...
jsault_dev_1  |
jsault_dev_1  | Host libadblockplus-dev
jsault_dev_1  |     # Set the port defined in your docker-compose.yml
jsault_dev_1  |     Port CHANGEME
jsault_dev_1  |     StrictHostKeyChecking no
jsault_dev_1  |     HostName 127.0.0.1
jsault_dev_1  |     User root
jsault_dev_1  |     ProxyCommand ssh distpartners-1.custom.eyeo.it -W %h:%p
jsault_dev_1  |
```

Follow the advice and add a section to your `~/.ssh/config` (`C:\Users\youruser\.ssh\config` on Windows) on your **workstation** (not the remote server). You should now be able to just ssh to the hostname defined in your ssh config, for example..

```
$ ssh libadblockplus-dev
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-51-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

This system has been minimized by removing packages and content that are
not required on a system that users do not log into.

To restore this content, you can run the 'unminimize' command.
Development image of registry.gitlab.com/eyeo/docker/libadblockplus_gitlab-runner:202004.1
Last login: Thu Apr 16 10:38:44 2020 from 172.27.0.1
```
The volume we defined has made the libadblockplus source code available in `/code` and
the OS contains the exact same libraries, compilers etc as used during the standard ci build process.

### Adding VS Code

A fairly common practice for some developers is using the [Remote - SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh) plugin
for Visual Studio Code to connect to a remote host. Our container is setup for just that and is already configured locally and should appear in the dropdown when selecting a host to connect to (ctrl+P, 'Remote SSH-Connect to host').

## Managing containers with docker-compose

In the example above we have created a container using `docker-compose up --build`. This starts the container and keeps the logs attached to the terminal session. If you `ctrl+c` out of this session then the container will be stopped. If you want to start the containers in the background add the `-d` flag (`docker-compose up --build -d`).

To delete the container created by docker-compose, use the `down` action (`docker-compose down`).

Note that any calls made to `docker-compose` should be done from the directory which contains the `docker-compose.yml` file you
wish to action. Full docs on docker compose are on [docker.com](https://docs.docker.com/compose/)

# Advanced usage

Docker and docker-compose are very powerful tools and let you do all sorts of crazy stuff. Here are some useful examples, feel free to add to this list via an MR.

- [Adding additional containers](docs/adding_additional_containers.md)
- [Connecting to your local Android device](docs/connect_to_local_android.md)
- [Creating and connecting to a new Android emulator](docs/run_android_emulator.md)
- [Building abpchromium with ccache](docs/build_adbpchromium.md)