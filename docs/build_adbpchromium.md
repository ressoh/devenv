## Building abpchromium using ccache

If you are working on abpchromium and want to use `ccache` to speed up the builds then its very easy to do.

Our standard build image for abpchromium contains `ccache` and our default `.gitlab-ci.yml` uses it to build. We can use our CI config to
generate a build script.

One minor gotcha is that `gclient sync` will fail unless both `/opt/ci/builds/abpchromium/` and `/opt/ci/builds/abpchromium/src/` are on the same filesystem. Due to how our current build process works, its required that the abpchromium repo is checked out into a directory called `src` and its parent directory is exposed as the mount point.

**tldr;**
On your host...

```shell
cd ~
mkdir abpchromium
cd abpchromium
git clone https://chromium-gitlab.eyeo.it/eyeo/abpchromium.git src
```

Here's our `docker-compose.yml`, it includes the correct `baseimage`, volume definitions, additional `pip` packages and environment variables. See comments inline for a brief explanation.

```yaml
version: '3'
services:
  jsault_testing:
    ports:
      - "127.0.0.1:5000:22"
    build:
      context: https://gitlab.com/eyeo/distpartners/devenv.git
      args:
        baseimage: registry.gitlab.com/eyeo/docker/abpchromium_gitlab-runner:202004.1
    volumes:
      - ~/.ssh:/ssh/:ro,Z
      # An external mount point for our ccache directory
      - /ccache:/opt/ci/ccache:Z
      # see note above, this needs to contain a src folder which contains our git repo
      - ~/abpchromium:/opt/ci/builds/abpchromium
    environment:
      ADDITIONAL_PACKAGES: "vim less"
      # A nice tool for reading yaml from the shell
      ADDITIONAL_PACKAGES_PIP: "niet"
      # Some more vars, copied from the .gitlab-ci.yml
      CCACHE_DIR: "/opt/ci/ccache"
      CCACHE_MAX_SIZE: "100G"
      CCACHE_COMPRESS: "1"
      CCACHE_CPP2: "yes"
      CCACHE_SLOPPINESS: "time_macros,include_file_mtime,include_file_ctime"
      CCACHE_COMPILERCHECK: "content"
      CCACHE_BASEDIR: "/opt/ci"
```

With the container up, lets use our CI config to run a local build. Our `docker-compose.yml` installed
an additional pip package called [`niet`](https://github.com/openuado/niet) which lets us easily parse
our `.gitlab-ci.yml` so we can run the same steps directly in our shell.

```shell
cd /opt/ci/builds/abpchromium/src

# optional, but this is what happens in CI
#rm -fr out/

# Run the before_script of our build job, only needed once per container
# this takes a while (it runs gclient sync)
source <(niet build_debug_chromium.before_script .gitlab-ci.yml)

# Actually perform our build
source <(niet build_debug_chromium.script .gitlab-ci.yml)
```

Thats it, your debug APK will be in `./out/Debug/apks/ChromePublic.apk` and your build has done the exact
same steps as what our CI system does, including the use of ccache.
