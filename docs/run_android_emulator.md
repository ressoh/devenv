## Creating an android emulator container

If you're building an android project and want to use an emulator (or [real device](connect_to_local_android.md)) to run tests
then thats also possible. The hardest part is deciding which docker image to use for your emulator device. Google provide some
[tools](https://github.com/google/android-emulator-container-scripts) to build a container but there are [other](https://github.com/budtmo/docker-android) options available.

For convenience a pre-built version of Android 10 (API level 29 R google_apis (x86_64)) is available in this repo (`registry.gitlab.com/eyeo/distpartners/devenv:q-google-x64-30.0.5`) and is used in the example below.

Once the relevant container image has been decided its just a case of adding it to your `docker-compose.yml` file with the relevant parameters (which are specific to the image used) and linking your development container to it. If using the image noted above then the `/dev/kvm` device has to be shared with the container as per the Google [docs](https://github.com/google/android-emulator-container-scripts#running-the-docker-image). Be careful when adding devices or enabling `priviledged` mode on containers, these present a security risk and only images from trusted sources should have these options enabled.

Our full `docker-compose.yml` file looks like this:

```
version: '3'
services:
  devenv_testing:
    ports:
      - "127.0.0.1:5000:22"
    build:
      context: https://gitlab.com/eyeo/distpartners/devenv.git
      args:
        baseimage: registry.gitlab.com/eyeo/docker/libadblockplus-android_gitlab-runner:202004.1
    volumes:
      - ~/.ssh:/ssh/:ro,Z
    environment:
      ADDITIONAL_PACKAGES: "net-tools vim adb nano"
    links:
      - "emulator"
  emulator:
    image: registry.gitlab.com/eyeo/distpartners/devenv:q-google-x64-30.0.5
    devices:
      - "/dev/kvm"
```

Once our containers have started (which can take a few minutes with the emulator) the android emulator can be accessed by its service name as defined in the yaml, `emulator` in this case.
We can just connect from our development container via adb:

```
root@b9f82f965344:~# adb connect emulator:5555
* daemon not running; starting now at tcp:5037
* daemon started successfully
connected to emulator:5555

root@b9f82f965344:~# adb devices
List of devices attached
emulator:5555	device

root@b9f82f965344:~# adb shell
generic_x86_64_arm64:/ $
```

The device can then be used as normal using your normal development tools (eg `gradle`)