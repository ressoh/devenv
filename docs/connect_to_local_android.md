## Connecting to local Android device from the container via adb

You're developing an Android app and want your container to have access to your phone. No problem, here's how to do it..

- install `adb` in the container if it's not already installed (`apt-get install -y adb` or add `adb` to your `ADDITIONAL_PACKAGES` environment variable)
- enable adb over tcp on your phone, `adb tcpip 5555` ([official docs](https://developer.android.com/studio/command-line/adb#wireless)). Disconnect the USB cable
- add this to your workstation's `~/.ssh/config` under the relevant hostname (the name of your container). Get the IP of your device from the phone (Settings -> Wifi)

  `RemoteForward 127.0.0.1:5555 IP.OF.YOUR.DEVICE:5555`
- Restart any ssh sessions to your container and run `adb connect 127.0.0.1:5555` from the container. It should show an unauthorized device and your device should prompt you to allow a new adb host. Accept the prompt and you are good to go!