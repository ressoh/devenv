## Adding additional containers (eg theia)

By default we only create a single container in our `docker-compose.yml` but we could create more. For example there is a web-based IDE called [theia](https://github.com/eclipse-theia/theia) by eclipse which could deploy pretty easily...

 - Create an additional `service` in the `docker-compose.yml` and include the `image` to use alongside any `volumes` and `environment` variables
 - Link your main container to the new container using the `links` definition so it can be accessed by name

Building on our example above, our new `docker-compose.yml` should look something like:

```
version: '3'
services:
  jsault_dev:
    ports:
      - "127.0.0.1:3000:22"
    build:
      context: https://gitlab.com/eyeo/distpartners/devenv.git
      args:
        baseimage: registry.gitlab.com/eyeo/docker/libadblockplus_gitlab-runner:202004.1
    volumes:
      - ~/.ssh:/ssh/:ro,Z
      - ~/libadblockplus:/code:Z
    environment:
      ADDITIONAL_PACKAGES: "vim net-tools nano"

    links:
      - "theia"
  theia:
    image: theiaide/theia
    volumes:
      - ~/libadblockplus:/home/project:Z
```

By default the `theia` container image exposes its web interface on port `3000` (see the [docs](https://github.com/theia-ide/theia-apps#theia-docker)) so we can access that directly from our main container:

```
root@18b244effa33:~# curl http://theia:3000
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="./bundle.js" charset="utf-8"></script>
</head>

<body>
  <div class="theia-preload"></div>
</body>

</html>
```

If we want to be able to access that from our desktop then its simply a case of adding a `LocalForward` to our ssh configuration for our host, for example..

```
LocalForward 127.0.0.1:3000 theia:3000
```

This allows us to access the web server running on the `theia` container from our desktop by going to 127.0.0.1:3000...

![screenshot](images/theia.png "Theia screenshot")